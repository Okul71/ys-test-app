const gulp = require('gulp'),
  browserify = require('browserify'),
  source = require('vinyl-source-stream'),
  buffer = require('vinyl-buffer'),
  webserver = require('gulp-webserver'),
  sourcemaps = require('gulp-sourcemaps'),
  uglify = require('gulp-uglify'),
  watchify = require('watchify'),
  sass = require('gulp-sass'),
  gulpCopy = require('gulp-copy');


function compile(watch) {
  var bundler = browserify('src/app/index.js', {
    debug: true,
    extensions: ['.js', '.jsx', '.json']
  });
  return bundler
    .transform('babelify', { presets: ['es2015', 'react'] })
    .bundle()
    .pipe(source('app.js'))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true })) // load browserify's sourcemaps
    .pipe(uglify()) // uglify js
    .pipe(sourcemaps.write('.')) // write .map files near scripts
    .pipe(gulp.dest('web/assets/js'));
}

gulp.task('js', function() {
  return compile();
});

gulp.task('webserver', function() {
  return gulp.src('./web')
    .pipe(webserver({
      livereload: true // reload browser page if something in BUILD_DIR updates
    }));
});

gulp.task('sass', function () {
  return gulp.src('./src/sass/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(gulp.dest('./web/assets/css'));
});

gulp.task('copy', function () {
  return gulp.src('./src/assets/**')
    .pipe(gulp.dest('./web/assets'));
});

gulp.task('watch:sass', function () {
  gulp.watch('./src/sass/**/*.scss', ['sass']);
});

gulp.task('watch:js', ['js'], function() {
  gulp.watch('./src/app/**/*.jsx', ['js']);
});

gulp.task('build', ['js', 'sass', 'copy']);

gulp.task('watch', [ 'webserver', 'watch:js','watch:sass']);
