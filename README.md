# YoungSkilled test app

This is example of React app build by gulp.

## Getting Started

### Prerequisites

Installed Node.js and NPM

### Installing

Clone repository
```
git clone https://Okul71@bitbucket.org/Okul71/ys-test-app.git
```

Install dependencies
```
npm install
```

Run gulp task to build project
```
gulp build
```

Run gulp webserver to start server on http://localhost:8000/
```
gulp webserver
```

Run gulp watch to start development
```
gulp watch
```
### TO DO

* Fix RWD style
* Start use good practices
