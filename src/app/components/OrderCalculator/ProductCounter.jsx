import React, {Component} from 'react';

const ProductCounter = ({increaseCounter, decreaseCounter, counter}) => {
    return (
        <div className="product">
            <div className="product-photo">
                <img
                    src="assets/img/pic-item.png" />
            </div>

            <div className="product-description">
                <h3>Acme Product</h3>
            <div className="product-description-buttons">
                <button
                    className="product-button-decrease"
                    onClick={decreaseCounter}
                >
                </button>

                <span className="product-description-counter">{counter}</span>

                <button
                    className="product-button-increase"
                    onClick={increaseCounter}
                >
                </button>
            </div>
                <div className="product-description-price">
                    <span>Price</span>
                <span className="price">$300.00</span>
                </div>
            </div>
        </div>
    );
};

export default ProductCounter;
