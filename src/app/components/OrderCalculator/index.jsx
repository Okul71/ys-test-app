import React, {Component} from 'react';
import { render } from 'react-dom';

import ProductCounter from './ProductCounter';
import OrderSummary from './OrderSummary';
import Cart from './Cart';

export default class Calculator extends Component {
    constructor() {
        super();
        this.state = {
            firstProduct: {
                quantity: 0,
                price: 300,
            },
            secondProduct: {
                quantity: 0,
                price: 300,
            },
            sumProduct: 0,
            sumPrice: 0,
        };
        this.increaseCounter = this.increaseCounter.bind(this);
        this.decreaseCounter = this.decreaseCounter.bind(this);
        this.countAllProduct = this.countAllProduct.bind(this);
        this.sumPrice = this.sumPrice.bind(this);
    }
    componentDidMount() {
        const elCart = document.getElementById('cart-counter');
        render(<Cart products={this.state.sumProduct} />, elCart);
    }
    componentDidUpdate() {
        const elCart = document.getElementById('cart-counter');
        render(<Cart products={this.state.sumProduct} />, elCart);
    }
    increaseCounter(product) {
        this.setState({
            [product]: {
                quantity: this.state[product].quantity + 1,
                price: 300, // to not mutate data
            }
        }, () => {
            this.countAllProduct();
            this.sumPrice();
        });
    }
    decreaseCounter(product) {
        if(this.state[product].quantity === 0) {
            return;
        }
        this.setState({
            [product]: {
                quantity: this.state[product].quantity - 1,
                price: 300, // to not mutate data
            }
        }, () => {
            this.countAllProduct();
            this.sumPrice();
        });
    }
    countAllProduct() {
        const sum = this.state.firstProduct.quantity + this.state.secondProduct.quantity;
        this.setState({
            sumProduct: sum
        })
    }
    sumPrice() {
        const firstProduct = parseInt(this.state.firstProduct.quantity) * parseInt(this.state.firstProduct.price);
        const secondProduct = parseInt(this.state.secondProduct.quantity) * parseInt(this.state.secondProduct.price);
        this.setState({
            sumPrice: firstProduct + secondProduct
        })
    }
    render() {
        return (
            <div>
                <h3>Your order</h3>
                <ProductCounter
                    increaseCounter={() => this.increaseCounter('firstProduct')}
                    decreaseCounter={() => this.decreaseCounter('firstProduct')}
                    counter={this.state.firstProduct.quantity}
                />
                <ProductCounter
                    increaseCounter={() => this.increaseCounter('secondProduct')}
                    decreaseCounter={() => this.decreaseCounter('secondProduct')}
                    counter={this.state.secondProduct.quantity}
                />
                <h3>Order Summary</h3>
                <OrderSummary
                    subtotal={this.state.sumPrice}
                />
            </div>
        )
    }
}
