import React, {Component} from 'react';

const OrdeSummary = ({subtotal}) => {
    const shipping = 20;
    const totalPrice = (subtotal !== 0) ? subtotal + shipping : 0;
    return (
        <div className="product-summary">
            <div className="product-summary-box">
                <span>Subtotal</span>
            <span className="product-summary-left">{`$ ${subtotal.toFixed(2)}`}</span>
            </div>

            <div className="product-summary-box">
                <span>Shipping</span>
            <span className="product-summary-left">{`$ ${shipping.toFixed(2)}`}</span>
            </div>
            <div className="product-summary-box product-summary-box--last">
                <span>Total</span>
            <span className="product-summary-left">{`$ ${totalPrice.toFixed(2)}`}</span>
            </div>
        </div>
    );
};

export default OrdeSummary;
