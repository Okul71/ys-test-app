import Validation from 'react-validation';
import React, {Component, PropTypes} from 'react';
import validationRules from './validationRules';

export default class OrderForms extends Component {
    constructor() {
        super();
        this.sendForm = this.sendForm.bind(this);
    }
    sendForm() {
        return alert('thanks for send form in React! :)');
    }
    render() {
        return <Validation.components.Form className="checkout-form">
            <h3 className="checkout-headline">Delivery Address</h3>
            <div className="form-group">
                <label>
                    First Name
                    <span className="dot"></span>
                    <Validation.components.Input className="checkout-form-input" type='text' value='' name='firstName' validations={['required']}/>
                </label>
            </div>
            <div className="form-group">
                <label>
                    Last Name
                    <span className="dot"></span>
                    <Validation.components.Input className="checkout-form-input" type='text' value='' name='lastName' validations={['required']}/>
                </label>
            </div>
            <div className="form-group">
                <label>
                    Email
                    <span className="dot"></span>
                    <Validation.components.Input className="checkout-form-input" value='email@email.com' name='email' validations={['required', 'email']}/>
                </label>
            </div>
            <div className="form-group">
                <label>
                    Phone
                    <span className="dot"></span>
                    <Validation.components.Input className="checkout-form-input" type='text' value='' name='phone' validations={['required']}/>
                </label>
            </div>
            <div className="form-group">
                <label>
                    Adress
                    <span className="dot"></span>
                    <Validation.components.Input className="checkout-form-input" type='text' value='' name='adress' validations={['required']}/>
                </label>
            </div>
            <div className="form-group">
                <label>
                    Adress Cont
                    <span className="dot"></span>
                    <Validation.components.Input className="checkout-form-input" type='text' value='' name='adressCount' validations={['required']}/>
                </label>
            </div>
            <div className="form-group">
                <label>
                    Zip Code
                    <span className="dot"></span>
                    <Validation.components.Input className="checkout-form-input" type='text' value='' name='zipCode' validations={['required']}/>
                </label>
            </div>
            <div className="form-group">
                <label>
                    City
                    <span className="dot"></span>
                    <Validation.components.Input className="checkout-form-input" type='text' value='' name='city' validations={['required']}/>
                </label>
            </div>
            <div className="form-group">
                <label>
                    State
                    <span className="dot"></span>
                    <Validation.components.Input className="checkout-form-input" type='text' value='' name='sate' validations={['required']}/>
                </label>
            </div>
            <div className="form-group choose-country">
                <label>
                    Country
                    <span className="dot"></span>
                    <div className="select-wrapper">
                        <Validation.components.Select name='country' className="checkout-form-select" value='' validations={['required']}>
                            <option value=''>Choose your city</option>
                                <option value="AF">Afghanistan</option>
                                <option value="AL">Albania</option>
                                <option value="DZ">Algeria</option>
                                <option value="AS">American Samoa</option>
                                <option value="AD">Andorra</option>
                        </Validation.components.Select>
                        <div className="select-wrapper-ico"></div>
                    </div>
                </label>

            </div>
            <div className="form-group">
                <h3 className="checkout-headline">Payments</h3>
            </div>
            <div className="form-group">
                <label>
                    Card number
                    <span className="dot"></span>
                    <Validation.components.Input
                        className="checkout-form-input"
                        type='number'
                        value=''
                        name='cardNumber'
                        validations={['required']}
                    />
                </label>
            </div>
            <div className="form-group">
                <label>
                    Card holder
                    <span className="dot"></span>
                    <Validation.components.Input className="checkout-form-input" type='text' value='' name='cardHolder' validations={['required']}/>
                </label>
            </div>
            <div className="form-group">
                <label>
                    Cvc
                    <span className="dot"></span>
                    <Validation.components.Input className="checkout-form-input" type='text' value='' name='cvc' validations={['required']}/>
                </label>
            </div>
            <div className="form-group">
                <Validation.components.Button onClick={this.sendForm} className="checkout-submit-button">Submit</Validation.components.Button>
            </div>
        </Validation.components.Form>;
    }
}
