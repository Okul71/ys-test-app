import React from 'react';
import { render } from 'react-dom';
import OrderCalculator from './components/OrderCalculator';
import OrderForms from './components/OrderForms';

window.onload = () => {
  const elOrderCalculator = document.getElementById('order-calculator');
  const elOrderForm = document.getElementById('order-form');

  render(<OrderCalculator />, elOrderCalculator);
  render(<OrderForms />, elOrderForm);
};
